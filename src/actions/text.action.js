import { BUTTON_ADD_TEXT_CLICKED, INPUT_TEXT_CHANGE } from "../constants/text.constant";

export const inputTextChangeAction = (inputValue) => {
    return {
        type: INPUT_TEXT_CHANGE,
        payload: inputValue
    }
}

export const btnAddTextClickedAction = () => {
    return {
        type: BUTTON_ADD_TEXT_CLICKED,
    }
}