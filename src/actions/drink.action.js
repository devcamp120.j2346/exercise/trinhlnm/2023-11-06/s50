import { BUTTON_LOC_DRINK_CLICKED, INPUT_DRINK_CHANGE } from "../constants/drink.constant"

export const inputDrinkChangeAction = (inputValue) => {
    return {
        type: INPUT_DRINK_CHANGE,
        payload: inputValue
    }
}

export const btnLocDrinkClickedAction = () => {
    return {
        type: BUTTON_LOC_DRINK_CLICKED,
    }
}