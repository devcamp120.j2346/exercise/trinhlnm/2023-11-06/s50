import { BUTTON_BUY_CLICKED } from "../constants/order.constant"

export const btnBuyClickedAction = (i) => {
    return {
        type: BUTTON_BUY_CLICKED,
        index: i
    }
}