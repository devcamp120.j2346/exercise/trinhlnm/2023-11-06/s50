import { BUTTON_LOC_DRINK_CLICKED, INPUT_DRINK_CHANGE } from "../constants/drink.constant";

const drinkListFull = [
    {drinkName: "Cà phê"},
    {drinkName: "Trà tắc"},
    {drinkName: "Pepsi"},
    {drinkName: "Cocacola"},
    {drinkName: "Trà sữa"},
    {drinkName: "Matcha"},
    {drinkName: "Hồng trà"},
    {drinkName: "Trà xanh kem cheese"},
    {drinkName: "Trà đá"},
];

const initialState = {
    inputString: "",
    drinkList: drinkListFull
}

const drinkReducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT_DRINK_CHANGE:
            state.inputString = action.payload;
            break;
        case BUTTON_LOC_DRINK_CLICKED:
            state.drinkList = drinkListFull.filter((e) => {
                return e.drinkName == state.inputString;
            });
            state.inputString = "";
        default:
            break;
    }

    return {...state};
}

export default drinkReducer;