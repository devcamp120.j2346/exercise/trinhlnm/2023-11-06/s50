import { BUTTON_ADD_TEXT_CLICKED, INPUT_TEXT_CHANGE } from "../constants/text.constant";

const initialState = {
    inputString: "",
    textList: []
}

const textReducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT_TEXT_CHANGE:
            state.inputString = action.payload;
            break;
        case BUTTON_ADD_TEXT_CLICKED:
            state.textList = [...state.textList, {
                text: state.inputString
            }];
            state.inputString = "";
            break;
        default:
            break;
    }

    return { ...state };
}

export default textReducer;