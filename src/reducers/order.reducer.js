import { BUTTON_BUY_CLICKED } from "../constants/order.constant"

const mobileList = [
    {
        name: "IPhone X",
        price: 900,
        amount: 0
    },
    {
        name: "Samsung S9",
        price: 800,
        amount: 0
    },
    {
        name: "Nokia 8",
        price: 650,
        amount: 0
    }
];

const initialState = {
    products: mobileList,
    total: 0
}

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case BUTTON_BUY_CLICKED:
            state.products[action.index].amount = state.products[action.index].amount + 1;
            state.total = state.total + state.products[action.index].price;
            break;
        default:
            break;
    }

    return { ...state };
}

export default orderReducer;