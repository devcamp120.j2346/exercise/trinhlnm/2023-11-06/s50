import { combineReducers } from "redux";
import textReducer from "./text.reducer";
import drinkReducer from "./drink.reducer";
import orderReducer from "./order.reducer";

const rootReducer = combineReducers({
   textReducer,
   drinkReducer,
   orderReducer
});

export default rootReducer;