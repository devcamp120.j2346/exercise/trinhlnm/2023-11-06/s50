import SimpleOrder from "./components/SimpleOrder";
import TableAddRow from "./components/TableAddRow";
import TableFilterRow from "./components/TableFilterRow";

function App() {
  return (
    <div>
      {/* <TableAddRow/> */}

      {/* <TableFilterRow/> */}

      <SimpleOrder/>
    </div>
  );
}

export default App;
