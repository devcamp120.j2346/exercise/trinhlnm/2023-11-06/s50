import { Box, Button, InputLabel, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, styled } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { btnLocDrinkClickedAction, inputDrinkChangeAction } from "../actions/drink.action";

const StyleTableCell = styled(TableCell)({
    border: "1px solid gray",
    fontWeight: "bold"
});

const TableFilterRow = () => {
    const dispatch = useDispatch();

    const { inputString, drinkList } = useSelector((reduxData) => {
        return reduxData.drinkReducer;
    });

    const inputChangeHandler = (event) => {
        dispatch(inputDrinkChangeAction(event.target.value));
    }

    const btnLocDrinkClicked = () => {
        dispatch(btnLocDrinkClickedAction());
    }

    return (
        <>
            <Box style={{ padding: "2rem", margin: "4rem", border: "1px solid #42a5f5", display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                <InputLabel style={{ fontWeight: "bold", color: "black" }}>Nhập nội dung lọc</InputLabel>
                <TextField variant="outlined" style={{ width: "60%" }} value={inputString} onChange={inputChangeHandler} />
                <Button variant="outlined" style={{ borderColor: "#8d6e63", color: "#8d6e63", textTransform: "capitalize" }} onClick={btnLocDrinkClicked}>Lọc</Button>
            </Box>

            <Box style={{ margin: "4rem" }}>
                <TableContainer>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead style={{ backgroundColor: "#8d6e63" }} sx={{}}>
                            <TableRow>
                                <StyleTableCell style={{ color: "white" }}>STT</StyleTableCell>
                                <StyleTableCell style={{ color: "white" }}>Nội dung</StyleTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {drinkList.map((e, i) => {
                                return (
                                    <TableRow>
                                        <StyleTableCell>{i + 1}</StyleTableCell>
                                        <StyleTableCell>{e.drinkName}</StyleTableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
        </>
    );
}

export default TableFilterRow;