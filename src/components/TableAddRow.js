import { Box, Button, InputLabel, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, styled } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { btnAddTextClickedAction, inputTextChangeAction } from "../actions/text.action";

const StyleTableCell = styled(TableCell)({
    border: "1px solid gray",
    fontWeight: "bold"
});

const TableAddRow = () => {
    const dispatch = useDispatch();

    const { inputString, textList } = useSelector((reduxData) => {
        return reduxData.textReducer;
    });

    const inputChangeHandler = (event) => {
        dispatch(inputTextChangeAction(event.target.value));
    }

    const btnAddTextClicked = () => {
        dispatch(btnAddTextClickedAction());
    }

    return (
        <>
            <Box style={{ padding: "2rem", margin: "4rem", border: "1px solid #42a5f5", display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                <InputLabel style={{ fontWeight: "bold", color: "black" }}>Nhập nội dung dòng</InputLabel>
                <TextField variant="outlined" style={{ width: "60%" }} value={inputString} onChange={inputChangeHandler} />
                <Button variant="outlined" style={{ borderColor: "#8d6e63", color: "#8d6e63" }} onClick={btnAddTextClicked}>Thêm</Button>
            </Box>

            <Box style={{ margin: "4rem" }}>
                <TableContainer>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead style={{ backgroundColor: "#8d6e63" }} sx={{}}>
                            <TableRow>
                                <StyleTableCell style={{ color: "white" }}>STT</StyleTableCell>
                                <StyleTableCell style={{ color: "white" }}>Nội dung</StyleTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {textList.map((e, i) => {
                                return (
                                    <TableRow>
                                        <StyleTableCell>{i + 1}</StyleTableCell>
                                        <StyleTableCell>{e.text}</StyleTableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
        </>
    );
}

export default TableAddRow;