import { Box, Button, Card, CardActions, CardContent, Grid, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { btnBuyClickedAction } from "../actions/order.action";

const SimpleOrder = () => {
    const dispatch = useDispatch();

    const { products, total } = useSelector((reduxData) => {
        return reduxData.orderReducer;
    });

    const onBtnBuyClicked = (i) => {
        dispatch(btnBuyClickedAction(i));
    }

    return (
        <>
            <Box style={{ padding: "2rem", margin: "2rem" }}>
                <Grid container>
                    {
                        products.map((e, i) => {
                            return (
                                <Grid item md={4}>
                                    <Card style={{ padding: "2rem", margin: "1rem" }}>
                                        <CardContent>
                                            <Typography variant="h5" component="div" style={{ marginBottom: "0.5rem" }}>
                                                {e.name}
                                            </Typography>
                                            <Typography variant="body2" style={{ marginBottom: "0.5rem" }}>
                                                Price: {e.price} USD
                                            </Typography>
                                            <Typography variant="body2">
                                                Quantity: {e.amount}
                                            </Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button variant="contained" color="success" size="small" style={{ textTransform: "capitalize" }}
                                                onClick={() => onBtnBuyClicked(i)}
                                            >Buy</Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                            );
                        })
                    }
                </Grid>

                <Box sx={{ fontWeight: "bold" }}>Total: {total} USD</Box>
            </Box>
        </>
    );
}

export default SimpleOrder;